for $name in distinct-values(doc('eprints.xml')//paper[contains(date_issue,'2014')]/author)
  let $years :=
   for $year in distinct-values(doc('eprints.xml')//paper/substring(date_issue,1,4))
   order by $year
   let $results := doc('eprints.xml')//paper[data(author) = $name and substring(date_issue,1,4) = $year]
   return <year id="{ if(number($year) > 0) then $year else concat('un','','known') }">{ count($results) }</year>
return if($years > 3) then <author name="{ $name }">{ $years }</author>
else ()