(for $name in distinct-values(doc('eprints.xml')//paper/author)
 let $entries := doc('eprints.xml')//paper[data(author) = $name]
 order by sum($entries/pages) descending
 return <author name="{ $name }">{ sum($entries/pages) }</author>)
[position() le 10]
