(for $name in distinct-values(doc('eprints.xml')//paper/author)
 let $numberTotal := count(doc('eprints.xml')//paper[data(author) = $name]/author) 
 let $numberSelf := count(doc('eprints.xml')//paper[data(author) = $name])
 let $difference := $numberTotal - $numberSelf
 order by $difference descending
 return <author name="{ $name }">{ $difference }</author>)[position() le 10]