for $x in distinct-values(//voyage/leftpage/destination/harbour)
  let $r := count(//voyage/leftpage[harbour=$x])
  let $a := count(//voyage/leftpage/destination[harbour=$x])
return <harbour name="{$x}"><nr-of-departures>{$r}</nr-of-departures><nr-of-destinations>{$a}</nr-of-destinations></harbour>