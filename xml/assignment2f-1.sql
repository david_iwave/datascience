select name,year,rating,a.nr_of_roles,d.nr_of_directors
from movie m
inner join (
	select mid,count(*) as nr_of_roles from acts group by mid
)
a on m.mid=a.mid
inner join (
	select mid,count(*) as nr_of_directors from directs group by mid
)
d on m.mid=d.mid