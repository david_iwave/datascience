SELECT
XMLELEMENT ( NAME "movie", 
XMLELEMENT (NAME "name", m.name ),
XMLELEMENT (NAME "year", m.year ),
XMLELEMENT (NAME "rating", m.rating ),
XMLAGG ( XMLELEMENT ( NAME "role", a.role ) )
)
FROM movie m
INNER JOIN acts a ON a.mid=m.mid
GROUP BY m.mid ;