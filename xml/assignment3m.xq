let $r :=
  for $x in doc('voc.xml')//voyage
  where exists($x/leftpage/master)
  let $z := $x/leftpage/master
  group by $z
  return <master name="{ $z }"  nrofvoyages="{ count($x) }"></master>
return $r[@nrofvoyages = max($r/@nrofvoyages)]
