SELECT
XMLELEMENT ( NAME "movie", XMLATTRIBUTES (m.mid AS "id"),
XMLELEMENT (NAME "name", m.name ),
XMLELEMENT (NAME "year", m.year ),
XMLELEMENT (NAME "rating", m.rating )
)
FROM movie m