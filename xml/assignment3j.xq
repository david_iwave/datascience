<totals>
<voyages>{ count(distinct-values(doc('voc.xml')//voyage))}</voyages>
<boats>{count(distinct-values(doc('voc.xml')//voyage/leftpage/boatname))}</boats>
<masters>{count(distinct-values(doc('voc.xml')//voyage/leftpage/master))}</masters>
</totals>