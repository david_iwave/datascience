select
replace(replace(
	CAST(
	XMLELEMENT ( NAME "movie", 
	XMLELEMENT (NAME "name", m.name ),
	XMLELEMENT (NAME "year", m.year ),
	XMLELEMENT (NAME "rating", m.rating ),
	XMLAGG ( XMLELEMENT ( NAME "director", d.name ) ) ,
	a.role_list )  as varchar)
,'&lt;','<'),'&gt;','>')
from movie m
left join (
	select mid,
	CAST (XMLAGG ( XMLELEMENT ( NAME "role", rrr.role ) ) as varchar) as role_list
	from acts rrr
	group by mid
)
a on m.mid=a.mid
left join (
	select mid,name from directs d INNER JOIN person p ON p.pid=d.pid group by mid,name
)
d on m.mid=d.mid
group by m.mid,a.role_list
order by m.name