for $x in distinct-values(doc('voc.xml')//voyage/leftpage/master)
  let $r := count(doc('voc.xml')//voyage/leftpage[master=$x])
return concat('<master name="',$x,concat('" nrofvoyages="',$r,'">'))