(for $x in doc('voc.xml')//voyage
where $x/leftpage/master='Jakob de Vries'
order by $x/leftpage/departure
return $x/number)[position() le 1]